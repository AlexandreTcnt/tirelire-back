from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.response import Response

from tirelire.models import Tirelire
from tirelire.serializers import TirelireSerializer


class GetAmount(generics.RetrieveAPIView):
    serializer_class = TirelireSerializer

    lookup_field = "owner"

    def get_queryset(self):
        return Tirelire.objects.all()


class SaveUp(generics.CreateAPIView):
    serializer_class = TirelireSerializer

    def get_queryset(self):
        return Tirelire.objects.all()

    def get_object(self):
        return Tirelire.objects.get(owner=self.request.data.get("owner"))

    def post(self, request, *args, **kwargs):
        data = request.data
        tirelire = self.get_object()
        serializer = TirelireSerializer(instance=tirelire, data=data, partial=True)
        serializer.is_valid(raise_exception=True)
        amount_to_add = serializer.validated_data.get("amount")
        tirelire.amount = float(tirelire.amount) + amount_to_add
        tirelire.save()
        return Response(serializer.data)


class CreateTirelire(generics.CreateAPIView):
    serializer_class = TirelireSerializer


class Spend(generics.DestroyAPIView):

    def get_object(self):
        return Tirelire.objects.get(owner=self.request.data.get("owner"))

    def delete(self, request, *args, **kwargs):
        return super().delete(request, args, kwargs)


class ListTirelire(generics.ListAPIView):

    serializer_class = TirelireSerializer

    def get_queryset(self):
        return Tirelire.objects.all()
