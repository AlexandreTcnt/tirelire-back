from rest_framework import serializers

from tirelire.models import Tirelire


class TirelireSerializer(serializers.ModelSerializer):

    amount = serializers.FloatField(required=False)

    class Meta:
        model = Tirelire
        fields = ["amount", "owner"]

    def create(self, validated_data):
        tirelire = Tirelire(amount=0, owner=validated_data.get("owner"))
        tirelire.save()
        return tirelire

    @staticmethod
    def validate_amount(value):
        if not isinstance(value, (int, float)):
            raise serializers.ValidationError(f"Error: expected float or int, got {type(value)!r}")
        if not value > 0:
            raise serializers.ValidationError(f"Error: expected a positive amount, got {value}")
        return value
