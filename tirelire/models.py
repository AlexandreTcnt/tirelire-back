from django.contrib.auth.models import User
from django.core.validators import MinValueValidator
from django.db import models


class Tirelire(models.Model):

    owner: str = models.TextField(primary_key=True)
    amount: float = models.DecimalField(validators=[MinValueValidator(0)], decimal_places=2, max_digits=10, default=0)
