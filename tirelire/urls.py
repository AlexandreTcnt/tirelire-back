from django.urls import path

from . import views

urlpatterns = [
    path("init/", views.CreateTirelire.as_view()),
    path("amount/<str:owner>", views.GetAmount.as_view()),
    path("save-up/", views.SaveUp.as_view()),
    path("spend/", views.Spend.as_view()),
    path("list/", views.ListTirelire.as_view())
]
