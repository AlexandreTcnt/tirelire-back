FROM python:3.11

WORKDIR /app

COPY requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY . /app

RUN python3 manage.py migrate
RUN python3 manage.py makemigrations

ENV DEBUG False

EXPOSE 8000

CMD ["gunicorn", "--bind", "0.0.0.0:8000", "tirelire_sc.wsgi:application"]
