# Tirelire back

This is the backend side of the application developed in Django 4.2 (Python3.11).

## Getting started

To build the application locally and dev, you need to create a .env file with the following environment variables:
DEBUG=True
ALLOWED_HOSTS=localhost, 127.0.0.1, [::1]

Then run the following commands :

`pip install requirements`

`python3 manage.py runserver`

The development server should start on port 8000. 

## Run with docker

To run the application with docker:

`docker build -t tirelire_back .`

`docker run -p 8000:8000 -it tirelire_back`

